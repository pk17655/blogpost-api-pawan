const jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
  const bearerHeader = req.headers.authorization;

  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];

    req.token = bearerToken;
    jwt.verify(req.token, 'secretkey', (err, result) => {
      if (err) {
        res.send(err);
      } else {
        next();
      }
    });
  }
}

module.exports = verifyToken;
