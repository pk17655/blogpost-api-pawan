const express = require('express');
const usersData = require('../../db/usersDb');

const router = express.Router();

// gets all users
router.get('/', (req, res) => {
  usersData
    .getAllUsers()
    .then((result) => {
      res.send(result);
    })
    .catch(() => {
      res.status(400).send({ msg: 'users not found' });
    });
});

module.exports = router;
