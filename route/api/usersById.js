const express = require('express');

const usersData = require('../../db/usersDb');

const router = express.Router();

// get single user
router.get('/:id', (req, res) => {
  const userId = req.params.id;
  usersData
    .getUserById(userId)
    .then((result) => {
      if (result.length === 0) {
        res.status(404).send({ msg: 'userId not found' });
      } else {
        res.send(result);
      }
    })
    .catch(() => {
      res.status(400).send({ msg: 'something went wrong' });
    });
});

module.exports = router;
