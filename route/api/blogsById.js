const express = require('express');

const blogData = require('../../db/blogsDb');
const usersData = require('../../db/usersDb');
const validation = require('../../validation/validateBlog');

const router = express.Router();


// create blog
router.post('/user/:id/blog', validation, async (req, res) => {
  const blog = {
    user_id: req.params.id,
    title: req.body.title,
    description: req.body.description,
  };

  try {
    const user = await usersData.getUserById(req.params.id);
    if (user.length === 0) {
      res.status(404).send({ msg: 'user not found' });
    } else {
      const newBlog = await blogData.createBlog(blog);
      const getNewBlog = await blogData.getBlogById(newBlog.insertId);
      res.send({ msg: ' new blog created ', blog: getNewBlog });
    }
  } catch (error) {
    res.send(error);
  }
});

// delete blogs of a user

router.delete('/user/:id/blogs', async (req, res) => {
  const userId = req.params.id;

  try {
    const user = await usersData.getUserById(userId);
    if (user.length === 0) {
      res.status(404).send({ msg: 'user not found' });
    } else {
      await blogData.deleteBlog(userId);
      res.send('blog deleted');
    }
  } catch (error) {
    res.send(error);
  }
});

// update blogs of a user

router.put('/user/:user_id/blog/:blog_id', validation, async (req, res) => {
  const userId = req.params.user_id;
  const id = req.params.blog_id;
  const blog = req.body;

  try {
    const user = await usersData.getUserById(userId);
    if (user.length === 0) {
      res.status(404).send({ msg: 'user not found' });
    } else {
      await blogData.updateBlog(blog, id);
      const getupdatedBlog = await blogData.getBlogById(id);
      res.send({ msg: ' blog updated ', blog: getupdatedBlog });
    }
  } catch (error) {
    res.status(400).send(error);
  }
});

module.exports = router;
