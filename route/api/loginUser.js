const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const usersData = require('../../db/usersDb');
const validation = require('../../validation/validateLoginDetail');

const router = express.Router();

router.post('/', validation, (req, res) => {
  const user = {
    email: req.body.email,
    password: req.body.password,
  };
  usersData.getPasswordByEmail(user.email).then((pass) => {
    if (bcrypt.compareSync(user.password, pass[0].password)) {
      const token = jwt.sign({ pass }, 'secretkey', { expiresIn: '2000s' });
      res.send({
        msg: 'user login successfull',
        token,
      });
    } else {
      res.send('login failed');
    }
  });
});


module.exports = router;
