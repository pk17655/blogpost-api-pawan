const express = require('express');
const bcrypt = require('bcrypt');
require('dotenv').config();
const usersData = require('../../db/usersDb');
const validation = require('../../validation/validateUser');

const router = express.Router();

router.post('/', validation, async (req, res) => {
  const pass = bcrypt.hashSync(req.body.password, 10);
  const user = {
    name: req.body.name,
    email: req.body.email,
    password: pass,
  };
  try {
    const newUser = await usersData.createUser(user);

    const getNewUser = await usersData.getUserById(newUser.insertId);
    res.send({ mag: 'user created successfully ', user: getNewUser });
  } catch (error) {
    res.send(error);
  }
  
});


module.exports = router;
