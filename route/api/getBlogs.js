const express = require('express');
const blogData = require('../../db/blogsDb');
const usersData = require('../../db/usersDb');

const router = express.Router();

// gets all blog
router.get('/blogs', (req, res) => {
  blogData
    .getAllBlogs()
    .then((result) => {
      res.send(result);
    })
    .catch(() => {
      res.status(400).send({ msg: 'blogs not found' });
    });
});
// get blog of single user
router.get('/user/:id/blogs/', async (req, res) => {
  const userId = req.params.id;

  try {
    const a = await usersData.getUserById(userId);
    if (a.length === 0) {
      res.status(404).send({ msg: 'user not found' });
    } else {
      const b = await blogData.getBlogByUserId(userId);
      if (b.length === 0) {
        res.status(404).send({ msg: 'no blogs found of this user' });
      } else {
        res.send(b);
      }
    }
  } catch (error) {
    res.send(error);
  }
});

// get blog by id
router.get('/blog/:id', (req, res) => {
  const { id } = req.params;

  blogData
    .getBlogById(id)
    .then((result) => {
      if (result.length === 0) {
        res.status(404).send({ msg: 'Id not found' });
      } else {
        res.send(result);
      }
    })
    .catch(() => {
      res.status(400).send({ msg: 'something went wrong' });
    });
});
module.exports = router;
