const mysql = require('./connection');

function promisifyQuery(query, blog) {
  return new Promise((resolve, reject) => {
    mysql.query(query, blog, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

function getAllBlogs() {
  return promisifyQuery('SELECT * FROM blogPost');
}

function getBlogByUserId(userId) {
  return promisifyQuery('SELECT * FROM blogPost where user_id = ?', userId);
}

function getBlogById(id) {
  return promisifyQuery('SELECT * FROM blogPost where id = ?', id);
}

function createBlog(blog) {
  return promisifyQuery('INSERT INTO blogPost SET ?', blog);
}

function deleteBlog(userId) {
  return promisifyQuery('delete  from blogPost where user_id = ?', userId);
}

function updateBlog(blog, id) {
  return promisifyQuery('UPDATE blogPost SET ? WHERE id = ?', [blog, id]);
}

module.exports = {
  getAllBlogs, getBlogByUserId, createBlog, getBlogById, deleteBlog, updateBlog,
};
