const mysql = require('./connection');

function promisifyQuery(query, user) {
  return new Promise((resolve, reject) => {
    mysql.query(query, [user], (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}


function getAllUsers() {
  return promisifyQuery('SELECT user_id, name, email FROM users');
}

function getUserById(userId) {
  return promisifyQuery('SELECT user_id, name, email FROM users where user_id = ?', userId);
}

function createUser(user) {
  return promisifyQuery('INSERT INTO users SET ?', user);
}


function getPasswordByEmail(user) {
  return promisifyQuery('select password from users where email = ?', user);
}



module.exports = {
  getAllUsers, getUserById, createUser, getPasswordByEmail
};
