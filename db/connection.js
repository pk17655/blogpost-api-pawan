/* eslint-disable no-console */
const mysql = require('mysql');
require('dotenv').config();

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: 'blogs',
});

connection.connect((err) => {
  if (err) throw err;
  else {
    console.log('database connected succesfully');
  }
});

module.exports = connection;
