
const express = require('express');
const logger = require('./middleware/logger');

const app = express();

app.use(logger);
app.use('/api/', require('./middleware/verifyToken'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.use('/users', require('./route/api/users'));
app.use('/user', require('./route/api/usersById'));
app.use('/', require('./route/api/getBlogs'));
app.use('/api/', require('./route/api/blogsById'));
app.use('/user/register', require('./route/api/registerUser'));
app.use('/user/login', require('./route/api/loginUser'));

const PORT = 1765;

app.listen(PORT, () => console.log(`Server started on prt ${PORT} `));
