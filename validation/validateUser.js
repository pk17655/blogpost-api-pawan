const Joi = require('joi');

const user = Joi.object({
  name: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ['com', 'net'] },
  }),
  password: Joi.string()
    .min(6)
    .required(),
});

function validateUser(req, res, next) {
  const result = user.validate(req.body);
  if (result.error) {
    return res.send(result.error);
    // return res.json('Error occured : Please  enter correct detail');
  }
  next();
}




module.exports = validateUser;
