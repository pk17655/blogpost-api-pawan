const Joi = require('joi');

const blog = Joi.object({
  title: Joi.string()
    .min(3)
    .max(70)
    .required(),
  description: Joi.string()
    .min(10)
    .required(),
});

function validateBlog(req, res, next) {
  const result = blog.validate(req.body);
  if (result.error) {
    return res.json(result.error);
  }
  next();
}
module.exports = validateBlog;
