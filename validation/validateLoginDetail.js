const Joi = require('joi');

const loginUser = Joi.object({
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ['com', 'net'] },
  }),
  password: Joi.string()
    .min(6)
    .required(),
});


function validateLoginDetail(req, res, next) {
  const result = loginUser.validate(req.body);
  if (result.error) {
    return res.send(result.error);
  }
  next();
}

module.exports = validateLoginDetail;
